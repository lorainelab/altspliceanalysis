#! /usr/bin/python

__author__="ACEnglish"
__date__ ="$Jun 15, 2009 1:06:56 PM$"
import ArabiTools as tool
import AltSpliceModeling as model

def findAltSplicingEvents(geneModels):
    """
    Taking a list of chromosome divided, locus sorted Gene Models, we're
    going to find all the alternative splicing events. This is going to be
    different from version 1.0 because we will return only  the pieces involved
    around the event. For example

    XXXX----XXXX
    XXXXX---XXXX

    Here we have the actual event around the 5th nucleotide position. But inorder
    to support it later, we'll need all four exons and both introns.

    XXXXXXXXXXXX
    XXXX---XXXXX

    Or three exons and one intron

    XXXXX--------XXXXXX
    XXXXX---XXX--XXXXXX

    or five exons and three introns.

    all introns i in G0 that overlap with exons e in G1, where each overlapping
    intron i lies at least 30 bases inside the 3-prime and 5-prime boundaries of G1.
    """

    ret = {}

    for chr in geneModels.keys():
        chrRet = []
        for locus in geneModels[chr].keys():
            #now we're looking at a single locus
            for x in range(0,len(geneModels[chr][locus])-1):
                geneX = geneModels[chr][locus][x]
                for y in range(x+1,len(geneModels[chr][locus])):
                    geneY = geneModels[chr][locus][y]
                    #comparing all gene Models with each other once
                    if geneX.overlaps(geneY,ignoreStarts=True) and not tool.sameSplicing(geneX,geneY):
                        chrRet.extend(findSpliceSites(geneX.sortedFeats(),geneY.sortedFeats()))
        ret[chr] = chrRet
        
    return ret

def findSpliceSites(model1Features, model2Features,BUFFER=30):
    #Overlapping intron must lie at least BUFFER
    #bases inside 3' and 5' of other gene Model's start.
    
    criticalStartForG2 = model1Features[0].getStart() + BUFFER
    criticalStartForG1 = model2Features[0].getStart() + BUFFER

    criticalEndForG2 = (model1Features[-1].getStart() + model1Features[-1].getLength()) - BUFFER
    criticalEndForG1 = (model2Features[-1].getStart() + model2Features[-1].getLength()) - BUFFER

    #truncate the ends that aren't available for AS
    #   "Overlapping Introns must lie at least BUFFER basses inside of 5' and 3'
    #   boundaries of Alternate gene models

    numFeaturesModel1 = len(model1Features)

    g1Truncate5prime = 0
    FivepTruncate = False

    g1Truncate3prime = numFeaturesModel1
    ThreepTruncate = False

    for intronNum in range(1,numFeaturesModel1/2 + 1,2):
        if model1Features[intronNum].getStart() >= criticalStartForG1 and (model1Features[intronNum].getStart() + model1Features[intronNum].getLength()) <= criticalEndForG1:
            FivepTruncate = True
        else:
            g1Truncate5prime += 2

        if model1Features[numFeaturesModel1-1-intronNum].getStart() >= criticalStartForG1 and (model1Features[numFeaturesModel1-1-intronNum].getStart() + model1Features[numFeaturesModel1-1-intronNum].getLength()) <= criticalEndForG1:
            ThreepTruncate = True
        else:
            g1Truncate3prime -= 2

        if FivepTruncate and ThreepTruncate:
            break;

    numFeaturesModel2 = len(model2Features)

    g2Truncate5prime = 0
    FivepTruncate = False

    g2Truncate3prime = numFeaturesModel2
    ThreepTruncate = False

    for intronNum in range(1,numFeaturesModel2/2 + 1,2):
        if model2Features[intronNum].getStart() >= criticalStartForG2 and (model2Features[intronNum].getStart() + model2Features[intronNum].getLength()) <= criticalEndForG2:
            FivepTruncate = True
        else:
            g2Truncate5prime += 2

        if model2Features[numFeaturesModel2-1-intronNum].getStart() >= criticalStartForG2 and (model2Features[numFeaturesModel2-1-intronNum].getStart() + model2Features[numFeaturesModel2-1-intronNum].getLength()) <= criticalEndForG2:
            ThreepTruncate = True
        else:
            g2Truncate3prime -= 2

        if FivepTruncate and ThreepTruncate:
            break;

    altModel = model.AltSpliceModel(model1Features[g1Truncate5prime:g1Truncate3prime],model2Features[g2Truncate5prime:g2Truncate3prime])
    return altModel.extractAS_Events()

