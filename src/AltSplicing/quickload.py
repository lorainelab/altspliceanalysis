"""Captures details of a Quickload IGB visualization server/site.

Using this is actually a bit awkward. It should be re-factored."""

import sys,os,re,urllib
import Mapping.Feature as f

# coordinate system codes
GFF_COORDSYS = 1
INTERBASE_COORDSYS = 2
INTERBASE = 2

# format codes
GFF_BASIC = 1
GFF_KEYVAL = 2
GFF_GROUPED = 3
TAIR_SGF = 4

class QuickloadSite:

    def __init__(self,d=None):
        self.set_dir(d)
        content = QuickloadContent(quickload=self)
        self.set_content(content)

    def set_dir(self,d):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        self._dir = d

    def get_dir(self):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        return self._dir

    def add_genome(self,genome):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        content = self.get_content()
        content.add_genome(genome)

    def get_genome(self,d):
        """
        Function:
        Example :
        Returns :
        Args    : d - genome directory
        """
        for genome in self.get_genomes():
            if genome.get_dir()==d:
                return genome

    def get_genomes(self):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        return self.get_content().get_genomes()
    
    def setup(self):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        setup_dir(self.get_dir())
        content = self.get_content()
        content.setup()
        
    def set_content(self,content):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        self._content = content

    def get_content(self):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        return self._content

    def read_data(self,fn):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        d = self.get_dir()
        if qldir.startswith('http://'):
            fh = urllib.urlopen(d + '/' + fn)
        else:
            fn = d + os.sep + fn
            if not os.path.exists(fn):
                sys.stderr.write("No such file: " + fn + ". Can't init_contents"+os.linesep)
                return None
            else:
                fh = open(qldir + os.sep + 'contents.txt')
        return fh

class QuickloadContent:

    def __init__(self,quickload=None):
        self._quickload = quickload
        self._genomes = []

    def set_quickload(self,ql):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        self._quickload = ql

    def get_quickload(self):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        return self._quickload

    def setup(self):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        for genome in self.get_genomes():
            genome.setup()
        self.write_contents_file()
    
    def initialize(self):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        ql = self.get_quickload()
        fh = ql.read_data('contents.txt')
        vals = {}
        while 1:
            line = fh.readline()
            if not line:
                break
            vals = line.rstrip().split('\t')
            d = vals[0]
            name = vals[1]
            genome = Genome(d=d,name=name,quickload=ql)
            self.add_genome(genome)

    def add_genome(self,genome):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        genomes = self.get_genomes()
        if not genome.get_dir() in map(lambda x:x.get_dir(),genomes):
            genomes.append(genome)

    def get_genomes(self):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        return self._genomes

    def write_contents_file(self):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        sep = '\t'
        ql = self.get_quickload()
        qdir = ql.get_dir()
        fn = qdir + os.sep + 'contents.txt'
        fh = open(fn,'w')
        for genome in self.get_genomes():
            name = genome.get_name()
            cdir = genome.get_dir()
            vals = [cdir,name]
            fh.write(sep.join(vals)+os.linesep)
        fh.close()

class Genome:

    def __init__(self,d=None,name=None,quickload=None):
        self.set_dir(d)
        self.set_name(name)
        self._syns = []
        self.add_synonym(name)
        self._annots = []
        self._quickload = quickload
        self._chrom_info = None
        
    def get_dir(self):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        return self._dir

    def set_dir(self,d):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        self._dir = d

    def set_name(self,name):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        self._name = name

    def get_name(self):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        return self._name

    def add_synonym(self,syn):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        if not syn:
            return
        if not syn in self.get_synonyms():
            self._syns.append(syn)

    def get_synonyms(self):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        return self._syns

    def get_annots(self):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        return self._annots
        
    def add_annots(self,fname):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        if not fname in self.get_annots():
            self._annots.append(fname)

    def add_seq(self,seqname=None,leng=None):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        ci = self.get_chrom_info()
        ci.add_seq(seqname,leng)

    def gets_seqs(self):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        return get_chrom_info().get_seqnames()

    def get_chrom_info(self):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        if not self._chrom_info:
            self.set_chrom_info(ChromInfo(genome=self,
                                          quickload=self.get_quickload()))
        return self._chrom_info

    def set_chrom_info(self,ci):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        self._chrom_info = ci

    def setup(self):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        setup_dir(self.get_quickload().get_dir() + os.sep + self.get_dir())
        self.write_annots_txt()
        chrominfo = self.get_chrom_info()
        chrominfo.setup()

    def write_annots_txt(self,fname=None):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        annots = self.get_annots()
        if len(annots) == 0:
            return
        if not fname:
            fn = self.get_quickload().get_dir() + os.sep + self.get_dir() + \
                 os.sep + 'annots.txt'
        fh = open(fn,'w')
        for annot in annots:
            fh.write(annot+os.linesep)
        fh.close()

    
    def get_quickload(self):
        """
        Function:
        Example :
        Returns :
        Args    :
        """        
        return self._quickload

class ChromInfo:

    def __init__(self,genome=None,quickload=None):
        self.set_genome(genome)
        self._seqs = []
        self._seq2len = {}
        self.set_quickload(quickload)

    def set_quickload(self,quickload):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        self._quickload = quickload

    def get_quickload(self):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        return self._quickload
    
    def set_genome(self,genome):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        self._genome = genome

    def get_genome(self):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        return self._genome

    def add_seq(self,seqname,leng):
        """
        Function:
        Example :
        Returns :
        Args    : seqname - name of the sequence, to be displayed
                            in IGB menu
                  leng - the size of the sequence, to be displayed in
                            IGB menu
        """
        seqnames = self.get_seqnames()
        if seqname not in seqnames:
            seqnames.append(seqname)
            sys.stderr.write("ChromInfo: added seq: " + seqname + os.linesep)
        self._seq2len[seqname]=leng

    def get_seqnames(self):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        return self._seqs

    def get_fname(self):
        """
        Function:
        Example :
        Returns :
        Args    :
        """        
        fn = self.get_quickload().get_dir() + os.sep + \
             self.get_genome().get_dir()\
             + os.sep + 'mod_chromInfo.txt'
        return fn

    def setup(self):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        self.write_chrom_info()

    def write_chrom_info(self,fn=None):
        """
        Function:
        Example :
        Returns :
        Args    :
        """
        if not fn:
            fn = self.get_fname()
        fh = open(fn,'w')
        sep = '\t'
        for seqname in self.get_seqnames():
            leng = repr(self._seq2len[seqname])
            vals = [seqname,leng]
            fh.write(sep.join(vals)+os.linesep)
        sys.stderr.write("Wrote: " + fn + os.linesep)
        fh.close()
    
class Annots:
    
    """Models a group of annotations associated with a single coordinate system, e.g., a chromosome."""

    def __init__(self,annotated=None):
        self._annotated = annotated

    def set_annotseq(self,annotseq):
        self._annotseq = annotseq

    def get_annotseq(self):
        return self._annotseq

class AnnotsFile(Annots):

    def __init__(self,fname=None,
                 format=None,annotated=None):
        Annots.__init__(self,display_id=display_id,annotated=annotated)
        self._fname = fname
        self._format = None

    def set_fname(self,fname):
        self._fname = fname

    def get_fname(self):
        return self._fname

    def set_format(self,format):
        self._format = format

    def get_format(self):
        return self._format


def setup_dir(d=None):
    """
    Function: If the directory exists, do nothing. If not, create it.
    Example : 
    Returns : 0 if successful, 1 if not.
    Args    : d - name of the directory
    """
    if not os.path.exists(d):
        os.mkdir(d)
        return 0
    else:
        if os.path.isfile(d):
            sys.stderr.write("Warning: Not making directory: " + d + ". A regular file by that name already exists."+os.linesep)
            return 1
        else:
            sys.stderr.write("Directory, link, or device called: " + d +" already exists."+os.linesep)
            return 0

def dummy():
    """
    Function:
    Example :
    Returns :
    Args    :
    """
    pass
