#! /usr/bin/python

__author__="ACEnglish"
__date__ ="$Jun 12, 2009 3:15:55 PM$"
import ArabiTools as tool
import UserInterface as ui
#import MySQLdb
import MappingTools as GPS
import SpliceSiteSearching as s3
import sys
import FileReader as reader
import FileWriter as ghostWriter
import time
import SupportAltSplicingEvents as supportGroup
import re
"""
This is the main file that will run all of the processing
and the goal is to have this one file be called to generate all the results
"""


def main(genesInFile,estsInFile,suffix,outPath,flank):
    #Preparing the screen
    #screen = ui.UserInterface()

    sys.stdout.write("Process Began %s\n" % time.asctime()); sys.stdout.flush()
   
    sys.stdout.write("Building Gene Models.  %s\n" % time.asctime()); sys.stdout.flush()
    if re.search('.bed',genesInFile):
    	myGenes = reader.readBed(genesInFile)
    elif re.search('.psl',genesInFile):
    	myGenes = reader.readPsl(genesInFile)
    else:
    	sys.stderr.write('ERROR! Gene file is in unknown format\n')
    	exit()
    myGenes = tool.sortByChromosome(myGenes)
	
    sys.stdout.write("Building EST Models.  %s\n" % time.asctime()); sys.stdout.flush()
    if re.search('.bed',estsInFile):
    	myESTs = reader.readBed(estsInFile)
    elif re.search('.psl', estsInFile):
    	myESTs = reader.readPsl(estsInFile)
    else:
    	sys.stderr.write('ERROR! ESTs file is in unknown format\n')
    	exit()

    myESTs = tool.sortByChromosome(myESTs)
    
    sys.stdout.write("Building Hash of Gene Loci. %s\n" % time.asctime()); sys.stdout.flush()
    locusGroups = GPS.createLocusHash(myGenes)

    #no database, we'll need to create and write the mapping file...
    sys.stdout.write("Building EST to Locus Map.  %s\n" % time.asctime()); sys.stdout.flush()
    locusCoordinateHash = GPS.createLocusCoordinateHash(locusGroups)
    estLocusMap = GPS.createESTtoLocusMap(myESTs,locusCoordinateHash)
	
    sys.stdout.write("Writing EST Supports Locus.  %s\n" % time.asctime()); sys.stdout.flush()
    fout = open(outPath+'ESTSupportsLocus_%s.txt'% (suffix) ,'w')
    fout.write("Locus\tEST\n")
    for chrom in estLocusMap.keys():
        for locus in estLocusMap[chrom].keys():
        	for est in estLocusMap[chrom][locus]:
        		 fout.write("%s\t%s\n" % (locus, est))
    fout.close();
    
    sys.stdout.write("Finding Alternative Splicing Events. %s\n" % time.asctime()); sys.stdout.flush()
    events = s3.findAltSplicingEvents(locusGroups)

    sys.stdout.write("Supporting and Creating AltSplicing Events. %s\n" % time.asctime()); sys.stdout.flush()
    altSplicingEvents = supportGroup.supportAltSplicingEvents(events,estLocusMap,flank)

    sys.stdout.write("Saving Alternative Splicing Objects. %s\n" % time.asctime()); sys.stdout.flush()
    ghostWriter.writeSplicingEvents(altSplicingEvents,outPath+"AltSplicing_%s.abt"%suffix)
    print "Finished %s" % time.asctime()
    

    #create dictionary to Hash out all genes to make locusID:(geneModel, geneModel...)
    #find alt splicing tuples using table
    #send to filter and write

def main2():
    """This is for creating the ESTSupportsGene table in est_genome"""
    sys.stdout.write("Process Began %s\n" % time.asctime()); sys.stdout.flush()

    sys.stdout.write("Building Gene Models.  %s\n" % time.asctime()); sys.stdout.flush()
    myGenes = reader.bed2feats(genesInFile)
    myGenes = tool.sortByChromosome(myGenes)

    sys.stdout.write("Building EST Models.  %s\n" % time.asctime()); sys.stdout.flush()
    myESTs = reader.psl2ESTs(estsInFile)
    myESTs = tool.sortByChromosome(myESTs)

    sys.stdout.write("Building Hash of Gene Loci. %s\n" % time.asctime()); sys.stdout.flush()
    locusGroups = GPS.createLocusHash(myGenes)


    #no database, we'll need to create and write the mapping file...
    sys.stdout.write("Building EST to Locus Map.  %s\n" % time.asctime()); sys.stdout.flush()
    locusCoordinateHash = GPS.createLocusCoordinateHash(locusGroups)
    estLocusMap = GPS.createESTtoLocusMap(myESTs,locusCoordinateHash)

    fout = open('ESTSupportsGeneModel.txt','w')

GPSUPPORTOPTION = False
	
if __name__ == "__main__":
    #regular run
    (genesInFile,estsInFile,suffix,outPath,gp,flank) = tool.getArgs(sys.argv[1:])
    
    #for next-gen sequencing RI support
    if gp:
    	supportGroup.RIGPSUPPORTOPTION = True
    #debug run
    #(genesInFile,estsInFile,suffix,outPath) = ('../../data/test.bed','../../data/test.psl','ns','')

    main(genesInFile,estsInFile,suffix,outPath,flank)

