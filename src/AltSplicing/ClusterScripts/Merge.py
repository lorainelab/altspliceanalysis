"""

Given an input of .abt files, concatenate them into a single file and output them to StdOut

"""
import sys

if __name__ == '__main__':

	#step 1, unite all the Loci across all files
	loci = {} # { Locus:[ [Ga ESTs], [Gp ESTs] ] }
	
	for file in sys.argv[1:]:
		fh = open(file, 'r')
		for line in fh.readlines():
			curDat = line.split('\t')
			curLoc = "~".join(curDat[:7])#only the ests are going to be different
			a=curDat[7].strip().split(',')
			b=curDat[8].strip().split(',')
			try:
				c = loci[curLoc]#This is a check so unsupported events are added
				if a != ['']:
					loci[curLoc][0].extend( a )
				if b != ['']:
					loci[curLoc][1].extend( b )
			except KeyError:
				loci[curLoc] = [ [], [] ]
				if a != ['']:
					loci[curLoc][0] = a
				if b != ['']:
					loci[curLoc][1] = b
		fh.close()
		
		
	for curLoc in loci.keys():
		#concatenation
		sys.stdout.write( "\t".join(curLoc.split('~')) + "\t" + ",".join(loci[curLoc][0]) + "\t" + ",".join(loci[curLoc][1]) + "\n")
		
	#Finished
