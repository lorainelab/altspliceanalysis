import sys
import curses,time

class UserInterface:

    def __init__(self):
        self.stdscr = curses.initscr()
        self.availableLine = 2
        curses.noecho()
        curses.cbreak()
        self.stdscr.keypad(1)
        self.window = curses.newwin(15,75,1,2)
        self.window.box()
        self.window.addstr(1,1,"Process began "+ time.asctime())
        self.window.refresh()
        self.resetCurser()

    def resetCurser(self):
        curses.noecho()
        self.window.box()
        self.window.move(14,74)
        self.window.refresh()

        
    def killScreen(self):
        curses.nocbreak()
        self.stdscr.keypad(0)
        curses.echo()
        curses.endwin()
        
    def updateProcess(self,process):
        """takes in a string that represents process currently being worked on"""
        self.window.addstr(self.availableLine,1,process+time.asctime())
        self.availableLine += 1
        self.resetCurser()

    def addLine(self,name):
        """creates a line for the progress to be printed on"""
        self.availableLine += 1
        self.window.addstr(self.availableLine,4, "% complete on " + name)
        self.resetCurser()
        self.window.refresh()
        
    def overrideLine(self,text):
        """overwrides all of what's on a line with text"""
        self.window.move(self.availableLine,0)
        self.window.clrtoeol()
        self.window.addstr(self.availableLine,1,text)
        self.window.box()
        self.resetCurser()
        self.window.refresh()

    def updatePercent(self,percent):
        """updates the percent on row"""
        if percent < 10:
            percent = "  %i"%(percent)
        elif percent < 100:
            percent =  " %i" %(percent)
        else:
            percent = "%i" %(percent)

        self.window.addstr(self.availableLine,1,percent)
        self.resetCurser()
        self.window.refresh()

    def getInput(self,prompt):
        """gets some input text from the user"""

        self.window.move(13,1)
        self.window.addstr(prompt)
        curses.echo()
        input = self.window.getstr(13,len(prompt)+1, 30)
        self.window.move(13,1)
        self.window.clrtoeol()
        self.resetCurser()
        self.window.refresh()

        return input

    def getChoice(self,prompt):
        """Gets yes or no choice from the user"""
        self.window.move(13,1)
        self.window.addstr(prompt)
        curses.echo()
        curses.beep()
        input = self.window.getch()
        self.window.move(13,1)
        self.window.clrtoeol()
        self.resetCurser()
        self.window.refresh()
        if input == ord('Y') or input == ord('y'):
            return True
        else:
            return False


if __name__ == "__main__":
    u = UserInterface()
    b = u.getInput("Maybe? ")
    if u.getChoice("More?! "):
        b += "YEA!"
        curses.beep()
        
    u.killScreen()
    print b