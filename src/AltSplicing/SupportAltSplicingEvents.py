#! /usr/bin/python

__author__="ACEnglish"
__date__ ="$Jun 18, 2009 1:45:47 PM$"
import ArabiTools as tool
import Feature
"""
This file will support all alternative splicing events and create Feature.SplicingEvents
"""
RIGPSUPPORTOPTION = False
def supportAltSplicingEvents(myEvents,myEST_LocusMap,FLANK):
    """
    Given a list of Alternative Splicing Events(AltSpliceModeling.AS_EventFeatures)
    and an estLocusMap(MappingTools.createESTtoLocusMap) we're going to find out
    which ests support which events and then create Feature.SplicingEvent
    """
    ret = []

    for chr in myEvents.keys():
        for curEvent in myEvents[chr]:
            type = curEvent.getType()

            geneAbsent = curEvent.getGeneAbsent()
            genePresent = curEvent.getGenePresent()
            
            #locus = curEvent.getGeneAbsent()[1].getDisplayId().split('.')[0]
            toks = curEvent.getGeneAbsent()[1].getDisplayId().split('.')
            locus = '.'.join(toks[0:len(toks)-1])
            try:
                locusESTs = myEST_LocusMap[chr][locus]
            except KeyError:
                #sometimes, we have no ests to support a locus...shit happens
                if type == 'RI':
                    ret.append(Feature.SplicingEvent(type,[],[],geneAbsent[1],genePresent[0]))
                elif type == 'DS':
                    ret.append(Feature.SplicingEvent(type,[],[],geneAbsent[1],genePresent[0],genePresent[1]))
                else:#AS
                    ret.append(Feature.SplicingEvent(type,[],[],geneAbsent[1],genePresent[2],genePresent[1]))
                continue;
                
            if type == 'RI':
                gASupport = []
                gPSupport = []

                for curEST in locusESTs:
                    for (prev,intron,next) in tool.featureIterator(curEST.sortedFeats()):
                        if intron != None and prev.getLength() >= FLANK and next.getLength() >= FLANK:
                            if intron.equals(geneAbsent[1], ignoreStrand = True):
                                gASupport.append(curEST.getDisplayId())

                    for curESTfeat in curEST.getFeats('exon'):
                        support = retainedIntronSupport(curESTfeat,geneAbsent[1],FLANK)
                        if support:
                            gPSupport.append(support)


                ret.append(Feature.SplicingEvent(type,gASupport,gPSupport,geneAbsent[1],genePresent[0]))
                
            else:#AS or DS -- MAclnSKAYA
                gASupport = []
                gPSupport = []

                for curEST in locusESTs:
                    for (prev,intron,next) in tool.featureIterator(curEST.sortedFeats()):
                        if intron != None and prev.getLength() >= FLANK and next.getLength() >= FLANK:
                            if intron.equals(geneAbsent[1], ignoreStrand = True):
                                gASupport.append(curEST.getDisplayId())

                            elif intron.equals(genePresent[1], ignoreStrand = True):
                                gPSupport.append(curEST.getDisplayId())
                                
                if type == 'DS':
                    ret.append(Feature.SplicingEvent(type,gASupport,gPSupport,geneAbsent[1],genePresent[0],genePresent[1]))
                else:
                    ret.append(Feature.SplicingEvent(type,gASupport,gPSupport,geneAbsent[1],genePresent[2],genePresent[1]))
    return ret

# curEST - something with method "getFeatures"
def retainedIntronSupport(myEST,myIntron,FLANK):
    """
    Find support for retained intron event's genePresent.

    XXX-----XXXX Ga
     xxxxxxxxxx [supports- contains]
    xxxxx       [supports- soft end  covers 20 bp before intron start and 20 bp after intron start
          xxxxx [supports- sott end  covers 20 bp before intron end and 20 bp after entron end
    """
    if myEST.contains(myIntron):#see lab notebook page for definition of RI support
        if (myEST.getStart() + FLANK) <= myIntron.getStart():
            if (myEST.getStart() + myEST.getLength() - FLANK) >= (myIntron.getStart() + myIntron.getLength()):
                 return myEST.getDisplayId()
    elif myEST.isSoftEnd():
        if (myEST.getStart() + FLANK) <= myIntron.getStart() and (myEST.getStart() + myEST.getLength() - FLANK) >= myIntron.getStart():
            return myEST.getDisplayId()
        elif (myEST.getStart() + FLANK) <= (myIntron.getStart() + myIntron.getLength()) and (myEST.getStart() + myEST.getLength() - FLANK) >= (myIntron.getStart()+myEST.getLength()):
            return myEST.getDisplayId()

    if RIGPSUPPORTOPTION and myIntron.contains(myEST,ignoreStrand=True):
    	return myEST.getDisplayId()
    	
    return None


if __name__ == "__main__":
    print "Hello World";
