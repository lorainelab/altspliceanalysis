
__author__="ACEnglish"
__date__ ="$Feb 7, 2009 1:52:21 PM$"
"""File containing all the helper methods that ArabiTag uses"""
import getopt
import sys

def sortByChromosome(models):
    """
    Function:   Sorts all the incoming gene model by their Chromosome
    Returns:    A dictionary: key - Chromosome, Value - List of models
    Args:       Models: A list of Compound___SeqFeatures
    """
    ret = {}
    for x in models:
        key = x.getSeqname()
        if ret.has_key(key):
            ret[key].append(x)
        else:
            ret.update({key: [x]})

    return ret


def sameSplicing(f1,f2,i=0,ignoreIntrons = False):
    """
    Function: determines if 2 DNASeqFeatureGroups are the result of alternative splicing
    Returns : boolean
    Args    : f1, f2 - CompoundDNASeqFeature objects
              i - integer 0 or 1, method uses recursion. i=1 stops recursion.
    """

    if f1.overlaps(f2):
        if ignoreIntrons or (numIntrons(f1)==numIntrons(f2) and numIntrons(f1) > 0):
            for x in f1.getFeats('intron'):
                for y in f2.getFeats('exon'):
                    if x.overlaps(y):
                        return False
            if i==0:
                return sameSplicing(f2,f1,i=1,ignoreIntrons=ignoreIntrons)
        else: return False
    else: return False

    return True

def numIntrons(f1):
    """
    Function: Counts the number of introns in DNASeqFeatureGroup
    Returns: int - number of introns
    Args: f1 - DNAseqFeatureGroup object
    """
    f1 = f1.getFeats('intron')
    if f1 == None:
        return 0

    return len(f1)


def featureIterator(model):
    """
    Function: turns a Compound___SeqFeature into an iterable.
    ***must be presorted if you want to iterate sequentially***
    Returns: All features in CompoundESTSeqFeature
    Args: ESTs CompoundESTSeqFeature to iterate

    CompoundESTSeqFeature:
    Exon1--Intron1--Exon2---Intron2--Exon3

    This will return
        Exon1--Intron1--Exon2
        Exon2--Intron2--Exon3

    CompoundESTSeqFeature:
        Exon1

    This will return
        Exon1--Intron1--Exon2
        Exon2--Intron2--Exon3
    """

    for i in range(0,len(model)-2,2):
        yield (model[i],model[i+1],model[i+2])
    if len(model) == 1:
        yield (model[0],None,None)

def usage():
    """Print a helpful message regarding usage of this program to stderr."""
    txt = """
    ArabiTag - Using ESTs to support the prevalance of Alternative Splicing.
    
    Commands:
    [-g|--genes filename.bed]
		The file that contains gene models.
		
    [-e|--ests filename.psl]
        The file that contains the EST alignments. 
        
    [-s|--suffix XY]
        The suffix that will distinguish the results. AltSplicing_XY.abt This is to help with file organization.
        
    [-o| --outdir filename] 
        The directory to write the results to. 
        
    [-p]
    	Allows ESTs without introns to support the gene present in retained intron events. (For short~ish reads like from Illumina experiments, where reads are unlikely to cross exon-exon boundaries)
    	
    [-f| --flank]
    	The amount of flank in base pairs an EST needs to support an alternative splicing event. Default is 20bp	
    
	"""
    sys.stderr.write(txt)

def getArgs(args):
    """Retrieve input and output file names as two-item tuple.
    Returns None in place of input or output file name when not
    specified in args. Exits with an error if illegal options
    given."""
    outfile = ""
    gp=False
    flank=20
    opts,args = getopt.gnu_getopt(args,"hpg:e:s:d:o:f:",["help","genes=","ests=","pgenes=","pests=","outfile=","createPickle","flank="])
    for opt, arg in opts:
        if opt in ("-h","--help"):
            usage()
            sys.exit()
        elif opt in ("-g","--genes"):
            genes = arg
        elif opt in ("-e","--ests"):
            ests = arg
        elif opt in ("-s","--suffix"):
            suffix = arg
        elif opt in ("-o","--outfile"):
            outfile = arg
        elif opt in ("-p"):
        	gp=True
        elif opt in ("-f", "--flank"):
        	flank = int(arg)
        else:
            raise getopt.GetoptError("Illegal option: " + opt)
    try:
        return (genes,ests,suffix,outfile,gp, flank)
    except Exception:
        sys.stderr.write("You are missing or have an illegal argument. See --help for argments list \n")
        sys.exit()

class IdentifiableRange:
    """An object that has a non-negative start position and a non-negative length.

    The coordinate system works like so:

   range A:
   start position: 0
   length: 5
   covers the first through the fifth base (x)
   |     |
    atgca atgca atgca atagc
         |       |
          range B: start position: 5
                   length 6
                   covers the sixth through the 11th base

Note that this coordinate system assigns numbers to the links, or
gaps between residues.  This has a number of advantages, such as
the fact that it can represent splice boundaries in a way that makes'
biological sense.  For example:

                 5' splice boundary:
                 start position: 15
                 length: 0
                    |
   nnnnn nnnnn nnngt nnnnn nnnnn nnnnn agnnn
                                      |
                                 3' splice boundary
                                 start position: 30
                                 length: 0

Note that this coordinate system differs from GFF, in which each
base is numbered, and the numbering begins with 1 at the first base
of the sequence.

This is sometimes called an 'interbase' numbering system.
    """

    def __init__(self,start,end,name):
        """

        Function: generates a new Range
        Returns : a new range
        Args    : start - start position
                  length - length of the range (number of residues)

        Start and length must be non-negative."""
        self.setStart(start)
        self.setEnd(end)
        self.setName(name)

    def setName(self,name):
        self.name = name

    def getName(self):
        return self.name

    def getStart(self):
        """
        Function: gets that start of this range
        Returns: the length of this range
        Args:
        """
        try:
            return self._start
        except AttributeError:
            return None

    def setStart(self,start):
        """
        Function: sets the start of this range
        Returns:
        Args: start - integer
        """
        if start < 0 and not start == None:
            raise Exception("Illegal value for start: must be non-negative.")
        self._start = start


    def getEnd(self):
        """
        Function: Gets the length of this range
        Returns: the length of this range
        Args:
        """
        try:
            return self._end
        except AttributeError:
            return None


    def setEnd(self,end):
        """
        Function: Sets the length of this range
        Returns: the length of this range
        Args: length - integer
        """

        if end < 0 and not end == None:
            raise Exception("Illegal value for length: must be non-negative.")
        self._end = end
        return self._end

    def overlaps(self,r, ignoreStarts = False):
        """
        Function: tests if r2 overlaps r1
        Args    : arg #1 = a range to compare this one to (mandatory)
                  ignoreStarts: Sometimes we don't care if they have the same
                  starts/lengths so we then set ignoreStarts to True
        Returns : true if the ranges overlap, false if not

        Overlap means: positions (residues) in common.

        e.g.,

    overlapping:

        s1        s1 + len1
         |-----------|
          nnnnn nnnnn nnnnn nnnnn nnnnn
               |-----------|
              s2        s2 + len2

              s1        s1 + len1
               |-----------|
          nnnnn nnnnn nnnnn nnnnn nnnnn
               |-----------|
              s2        s2 + len2

    not overlapping:

        s1        s1+len1
         |-----------|
          nnnnn nnnnn nnnnn nnnnn nnnnn
                     |-----------|
                    s2        s2 + len2

        """
        s1 = self.getStart()
        len1 = self.getEnd()
        s2 = r.getStart()
        len2 = r.getEnd()

        if ignoreStarts:
            return not ( s2 >= s1 + len1 or s1 >= s2 + len2)
        else:
            return not ( s2 >= s1 + len1 or s1 >= s2 + len2) and not (s1 == s2 and len1 == len2)

    def contains(self,r):
        """
        Function: tests if r1 contains r2
        Args    : arg #1 = a range to compare this one to (mandatory)
        Returns : true if this contains r2, false if not

    contained:

        s1                     s1 + len1
         |------------------------|
          nnnnn nnnnn nnnnn nnnnn nnnnn
               |-----------|
              s2        s2 + len2

    not contained:

        s1               s1+len1
         |-----------------|
          nnnnn nnnnn nnnnn nnnnn nnnnn
                     |-----------|
                    s2        s2 + len2

        """
        s1 = self.getStart()
        len1 = self.getLength()
        s2 = r.getStart()
        len2 = r.getLength()
        return s2 >= s1 and s2 + len2 <= s1 + len1

    def equals(self,r):
        """
        Function: tests if r2 equals r1 (they have the same coordinates)
        Args    : arg #1 = a range to compare this one to (mandatory)
        Returns : true if the ranges are equal, false if not
        """
        s1 = self.getStart()
        len1 = self.getLength()
        s2 = r.getStart()
        len2 = r.getLength()
        return s1 == s2 and len1 == len2
 
    def getLength(self):
        """
	Function: Returns the length of the range
	"""
	return (self.getEnd()-self.getStart())
