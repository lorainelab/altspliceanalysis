"""Classes for modeling features on DNA sequence and other biological molecules."""
import re
import string
    

class IllegalStrandValueError(Exception):
    pass

class Range:
    """An object that has a non-negative start position and a non-negative length.

    The coordinate system works like so:

   range A:
   start position: 0
   length: 5
   covers the first through the fifth base (x) 
   |     |
    atgca atgca atgca atagc
         |       |
          range B: start position: 5
                   length 6
                   covers the sixth through the 11th base

Note that this coordinate system assigns numbers to the links, or
gaps between residues.  This has a number of advantages, such as
the fact that it can represent splice boundaries in a way that makes'
biological sense.  For example:

                 5' splice boundary:
                 start position: 15
                 length: 0
                    |
   nnnnn nnnnn nnngt nnnnn nnnnn nnnnn agnnn
                                      |
                                 3' splice boundary
                                 start position: 30
                                 length: 0

Note that this coordinate system differs from GFF, in which each
base is numbered, and the numbering begins with 1 at the first base
of the sequence.

This is sometimes called an 'interbase' numbering system.
    """

    def __init__(self,start=None,length=None,**kwargs):
        """
        
        Function: generates a new Range 
        Returns : a new range
        Args    : start - start position
                  length - length of the range (number of residues)

        Start and length must be non-negative."""
        self.setStart(start)
        self.setLength(length)

    def getStart(self):
        """
        Function: gets that start of this range
        Returns: the length of this range
        Args:
        """
        try:
            return self._start
        except AttributeError:
            return None

    def setStart(self,start):
        """
        Function: sets the start of this range
        Returns:
        Args: start - integer
        """
        if start < 0 and not start == None:
            raise Exception("Illegal value for start: must be non-negative.")
        self._start = start

            
    def getLength(self):
        """
        Function: Gets the length of this range
        Returns: the length of this range
        Args:
        """
        try:
            return self._length
        except AttributeError:
            return None


    def setLength(self,length):
        """
        Function: Sets the length of this range
        Returns: the length of this range
        Args: length - integer
        """

        if length < 0 and not length == None:
            print length
            raise Exception("Illegal value for length: must be non-negative.")
        self._length = length
        return self._length

    def overlaps(self,r, ignoreStarts = False):
        """
        Function: tests if r2 overlaps r1
        Args    : arg #1 = a range to compare this one to (mandatory)
                  ignoreStarts: Sometimes we don't care if they have the same
                  starts/lengths so we then set ignoreStarts to True
        Returns : true if the ranges overlap, false if not

        Overlap means: positions (residues) in common.

        e.g.,

    overlapping:

        s1        s1 + len1
         |-----------|
          nnnnn nnnnn nnnnn nnnnn nnnnn
               |-----------|
              s2        s2 + len2

              s1        s1 + len1
               |-----------|
          nnnnn nnnnn nnnnn nnnnn nnnnn
               |-----------|
              s2        s2 + len2

    not overlapping:

        s1        s1+len1
         |-----------|
          nnnnn nnnnn nnnnn nnnnn nnnnn
                     |-----------|
                    s2        s2 + len2
                    
        """
        s1 = self.getStart()
        len1 = self.getLength()
        s2 = r.getStart()
        len2 = r.getLength()

        if ignoreStarts:
            return not ( s2 >= s1 + len1 or s1 >= s2 + len2)
        else:
            return not ( s2 >= s1 + len1 or s1 >= s2 + len2) and not (s1 == s2 and len1 == len2)

    def contains(self,r):
        """
        Function: tests if r1 contains r2
        Args    : arg #1 = a range to compare this one to (mandatory)
        Returns : true if this contains r2, false if not

    contained:

        s1                     s1 + len1
         |------------------------|
          nnnnn nnnnn nnnnn nnnnn nnnnn
               |-----------|
              s2        s2 + len2
              
    not contained:

        s1               s1+len1
         |-----------------|
          nnnnn nnnnn nnnnn nnnnn nnnnn
                     |-----------|
                    s2        s2 + len2
        
        """
        s1 = self.getStart()
        len1 = self.getLength()
        s2 = r.getStart()
        len2 = r.getLength()
        return s2 >= s1 and s2 + len2 <= s1 + len1
        
    def equals(self,r):
        """
        Function: tests if r2 equals r1 (they have the same coordinates)
        Args    : arg #1 = a range to compare this one to (mandatory)
        Returns : true if the ranges are equal, false if not
        """
        s1 = self.getStart()
        len1 = self.getLength()
        s2 = r.getStart()
        len2 = r.getLength()
        return s1 == s2 and len1 == len2

class SeqFeature(Range):
    """A feature on a sequence, such as protein, DNA, or RNA"""     

    def __init__(self, producer=None, seqname=None, type=None, display_id=None, **kwargs):
        Range.__init__(self,**kwargs)
        self.setSeqname(seqname)
        self.setType(type)
        self.setProducer(producer)
    	self.setDisplayId(display_id)

    def getDisplayId(self):
        """
        Function: get the display_id, a string
        Returns : the display_id, such as Genbank Accession
        Args    : 

        If the display_id has not been set, returns None.
        """
        try:
            return self._display_id
        except AttributeError:
            return None

    def setDisplayId(self, display_id):
        """
        Function: set the display_id, a string
        Returns : the display_id, such as Genbank Accession
        Args    : display_id - string
        """
        self._display_id = display_id

    def __str__(self):
        return self._display_id

    
    def setSeqname(self,seqname):
        """
        Function: set name of annotated sequence 
        Args    : a string, or None
        Returns : 
        """
        self._seqname = seqname

    def getSeqname(self):
        """
        Function: get name of annotated sequence 
        Args    : 
        Returns : the name of the annotated sequence or None if not set
        """
        try:
            return self._seqname
        except AttributeError:
            return None

    def setProducer(self,producer):
        """
        Function: set the producer of the annotation
        Args    : a string, or None
        Returns : 

        For example, producer corresponds to the producer (2nd) field
        in a GFF format file.
        """
        self._producer= producer

    def getProducer(self):
        """
        Function: get the producer of the annotation
        Args    : 
        Returns : the name of the producer or None if not set

        For example, producer corresponds to the producer (2nd) field
        in a GFF format file.
        """
        try:
            return self._producer
        except AttributeError:
            return None
    
    def setType(self,type):
        """
        Function: set feature type
        Args    : a string, or None
        Returns : 

        Return or set the type of SeqFeature this is.

        For example, type corresponds to the feature (3rd) field in a
        GFF format file.
        """
        self._type = type

    def getType(self):
        """
        Function: get feature type
        Args    : 
        Returns : the type of feature this is or None if not set

        Return or set the type of SeqFeature this is.

        For example, type corresponds to the feature (3rd) field in a
        GFF format file.
        """
        try:
            return self._type
        except AttributeError:
            return None

class Taggable:
    """Class that allows objects that inherits from it to tag objects to it."""

    def __init__(self,**kwargs):
        self.Tags = []
        self.hasTags(False)

    def addTag(self,est,force = 0):
        """Adds tag"""
        if not self.Tags.__contains__(est) or force:
            self.Tags.append(est)

    def getTags(self):
        """Returns all tags attached"""
        return self.Tags

    def hasTags(self,t):
        """Set if this has been tagged already or not"""
        self.beenTagged = t

    def hasBeenTagged(self):
        return self.beenTagged


class DNASeqFeature(SeqFeature,Taggable):
    """A feature on a DNA sequence."""

    def __init__(self, strand=None,
                 **kwargs):
        SeqFeature.__init__(self,**kwargs)
        Taggable.__init__(self,**kwargs)
        self.setSoftEnd(False)
        self.setStrand(strand)
                

    def getStrand(self):
        """
        Function: get the strand of this range
        Returns : the strand of this range
        Args    : 
        """
        try:
            return self._strand
        except AttributeError:
            return None

    def setStrand(self,strand):
        """
        Function: set the strand of this range
        Returns : 
        Args    : strand - 1 or -1
        """
        if strand == 1 or strand == -1 or strand == None:
            self._strand = strand
        else:
            raise IllegalStrandValueError(strand)

    def setSoftEnd(self,hard):
        """First and last SeqFeature has a hard End"""
        self.hardEnd = hard
    def isSoftEnd(self):
        return self.hardEnd

    def contains(self,feat,ignoreStrand=False):
        """
        Function: tests if this contains another DNASeqfeature and
                  both are on the same strand
        Args    : arg #1 = a DNASeqFeature to compare this one to (mandatory)
        Returns : true if this contains the other, false if not
        """
        if ignoreStrand: return Range.contains(self,feat)
        
        my_strand = self.getStrand()
        other_strand = feat.getStrand()
        if not (my_strand and other_strand):
            raise Exception("Error: strand not set.")
        if my_strand != other_strand:
            return False
        
        return Range.contains(self,feat)
        
    def equals(self,feat,ignoreStrand = False):
        """
        Function: tests if this has the same coordinates and strand as
                  another DNASeqFeature
        Args    : arg #1 = a DNASeqFeature to compare this one to (mandatory)
                  r - boolean: if True, ignore Strand
        Returns : true if the coordinates and strand are equal, false if not
        """
        if not ignoreStrand:
            my_strand = self.getStrand()
            other_strand = feat.getStrand()
            if not (my_strand or other_strand):
                raise Exception("Error: strand not set.")
            if my_strand != other_strand:
                return False
        else:
            return Range.equals(self,feat)
        

    def isFivePrimeOf(self,other):
        """
        Function: determine whether this is five prime of another
                  DNASeqFeature
                  Example : if feat.is_five_prime_of(other):
                                print feat.display_name() +  ' is 5'!';
           
        Returns : True or False
        Args    : a DNASeqFeature or a Stranded Range

        Determine whether this DNASeqFeature is located 5-prime
        of the given DNASeqFeature or Stranded Range

        See docs on is_three_prime_of for details.
        """
        strand = self.getStrand()
        if not strand == other.getStrand():
            raise Exception("Can't compare.  Strand not the same.")
        my_start = self.getStart()
        my_stop = self.getStart() + self.getLength()
        other_start = other.getStart()
        other_stop = other.getLength() + other_start
        if strand == 1:
            if my_start < other_start:
                return True
        else:
            if my_stop > other_stop:
                return True
        return False


    def isThreePrimeOf(self,other):
        """
        Function: determine whether this is three prime of another
                  DNASeqFeature
        Returns : True or False
        Args    : a DNASeqFeature or a Stranded Range

        Determine whether this DNASeqFeature contains positions
        (bases) located at positions 3-prime of (left of, if +1
        strand, right of if -1 strand) of every position (bases)
        in the given DNASeqFeature.

        The two must be on the same strand to be compared in this
        way.
        
        Here's an example:

        xxxxxx
                yyyyyyyyyy
        5'                               3'
        ---------------------------------- +1 strand
        ---------------------------------- -1 strand
        3'                               5'
                   aaaaaaaaaaaa
                                 bbbbbbb

        y.is_three_prime_of(x) returns: True
        a.is_three_prime_of(b) returns: True
        x.is_three_prime_of(a) returns: An error!  They are not
        on the same strand.

        """
        strand = self.getStrand()
        if not strand == other.getStrand():
            raise Exception("Can't compare.  Strand not the same.")
        my_start = self.getStart()
        my_stop = self.getStart() + self.getLength()
        other_start = other.getStart()
        other_stop = other.getLength() + other_start
        if strand == 1:
            if my_stop > other_stop:
                return True
        else:
            if my_start > other_start:
                return True
        return False
        
class CompoundSeqFeature(DNASeqFeature):
    """A SeqFeature that contains other SeqFeatures.  A SeqFeature that is a collection of other SeqFeatures."""

    def __init__(self, feats=None, start=None, length=None, **kwargs):
        DNASeqFeature.__init__(self,start=start,
                                length=length,
                                **kwargs)

        self._feats_dict = {}
        self._feats_list = []
        self._sorted = {}
        min_start = None
        max_end = None
        if feats:
            for feat in feats:
                self.addFeat(feat,1)
                if start is None and length is None:
                    if min_start is None and max_end is None:
                        min_start = feat.getStart()
                        max_end = feat.getStart() + feat.getLength()
                    else:
                        if feat.getStart() < min_start:
                            min_start = feat.getStart()
                        if feat.getStart() + feat.getLength() > max_end:
                            max_end = feat.getStart() + feat.getLength()
            if start is None and length is None:
                start = min_start
                length = max_end - min_start
        for key in self._feats_dict.keys():
            self._sorted[key] = False
        self._sorted['all'] = False
        
    def getFeats(self,type=None):
        """
        Function: get subfeats of certain type, if given
        Returns : all the feats or just feats of the given type
        Args    : nothing, or a type

        Return a list containing the requested subfeats
        in whatever order they are stored internally.

        If no subfeatures have been added, then returns empty list.
        """
        all_feats = self._feats_dict # a dictionary, types are keys
        all_list = self._feats_list # a list
        if type:
            try:
                type_feats = all_feats[type]
                return type_feats # a list
            except KeyError:
                return [] # no feats of that type
        else:
            return all_list

    def addFeat(self,feat,safe=0):
        """
        Function: add a SeqFeature to this CompoundSeqFeature
        Returns : 
        Args    : a SeqFeature or derived class
                  safety flag (default 0)

        Add a SeqFeature to the list.  If the safe flag is non-zero and not
        None, do not check whether the feature has already been added.
        """
        type = feat.getType()
        if not type:
            raise Exception("Can't add feat of unknown type.")
        all_dict = self._feats_dict # a dictionary, types are keys
        all_list = self._feats_list # a list
        try:
            type_list = all_dict[type]
        except KeyError:
            type_list = []
            all_dict[type] = type_list
        if safe:
            type_list.append(feat)
            all_list.append(feat)
        else:
            if feat not in type_list:
                type_list.append(feat)
                self._sorted[type] = False
            if feat not in all_list:
                all_list.append(feat)
                self._sorted['all'] = False
                
    def sortedFeats(self,type=None):
        """
        Function: 
        Returns : a list of all the feats or just feats of the given type
                  sorted by start position
        Args    : nothing, or a type

        Return a list containing the requested subfeats
        sorted (low to high) by start position."""


        feats = self.getFeats(type=type)
        if feats:
            feats.sort(lambda x,y:x.getStart()-y.getStart())
        return feats
       
class SplicingEvent:
    """A pair of DNASeqFeatures that have disagreement region"""

    def __init__(self,spliceType,gASupport,gPSupport,intronOverlap,exonOverlap,intronAlt=None):

        self.setIntronOverlap(intronOverlap)
        self.setGAbsentSupport(gASupport)
        self.setExonOverlap(exonOverlap)
        self.setIntronAlt(intronAlt)
        self.setGPresentSupport(gPSupport)
        self.setType(spliceType)

    def getExonOverlap(self):
        """returns the exon of the splicing event"""
        return self.exon
    def setExonOverlap(self,e):
        """sets the exon of the splicing event"""
        self.exon = e

    def getIntronOverlap(self):
        """returns the Intron of the splicing event"""
        return self.intron
    def setIntronOverlap(self,i):
        """sets the Intron of the splicing event"""
        self.intron = i

    def getIntronAlt(self):
        """returns the i_alt region of the splicing event"""
        return self.intronAlt
    def setIntronAlt(self,i):
        """Sets the i_alt of the splicing event"""
        self.intronAlt = i

    def setType(self,bool):
        self.spliceType = bool

    def getType(self):
        """If the size and location of disagreement region is equal to entire intron
           then the alternative splicing difference should be tagged retained intron."""
        return self.spliceType

    def setGAbsentSupport(self,s):
        self.gaSupport = s
    def getGAbsentSupport(self):
        return self.gaSupport

    def setGPresentSupport(self,s):
        self.gpSupport = s
    def getGPresentSupport(self):
        return self.gpSupport

    def equals(self,other):
        """checks if two splicing events are the same thing"""
        return self.getExonOverlap() == other.getExonOverlap() and self.getIntronOverlap() == other.getIntronOverlap() 

def zdummy():
    """
    Function:
    Returns :
    Args    :
    """
    pass
