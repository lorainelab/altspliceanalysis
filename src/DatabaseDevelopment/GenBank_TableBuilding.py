#! /usr/bin/python

__author__="ACEnglish"
__date__ ="$May 1, 2009 11:40:01 AM$"

import re
import gzip
import sys
import pickle
"""
This file is going to take all the output from Arabidopsis_dbEST_new.py and creates a huge XML file that will hold each library, it's attributes, and it's members.

The xml format:

<library name="required" desc="also required" genbank="TRUE">
	<attribute label="label">Value</attribute>
	<attribute label="label">Value</attribute>
	<attribute label="label">Value</attribute>
	<members>
		<est name="estName" gi="genbank" acc="acc" />
		<est name="estName" gi="genbank" acc="acc" />
		<est name="estName" gi="genbank" acc="acc" />
		<est name="estName" gi="genbank" acc="acc" />
	</members>
</library>

"""

def readfile(fn=None):
	"""
	Function: Open a file for reading. 
	Returns : a file object
	Args	: fn - the name of the file

	It can be a plain text file or a
	file compressed using gzip. If the latter,
	the file name must terminate in characters
	.gz
	"""
	import gzip					  
	if not fn:
		sys.stderr.write("Error: need an input file\n")
		return None
	if fn.endswith('.gz'):
		fh=gzip.GzipFile(fn)
	else:
		fh=open(fn,'r')
	return fh
	
def extractDBinformation(infile):
	"""
	infile should be from Arabidopsis_dbEST_new.py (i.e. GenBank Format)
	
	Time to build the database.
	Step one is to take out all the information we need

	I'm going to build a list, where each line is a single EST record with the following format
	gi`accession`dbESTlibID`attribute1:attr1value`attribute2:attr2value`...`attributeN:attrNvalue`

	#I use back ticks because they are nowhere to be found in the file I'm parsing.
	"""
	ret = []#The return list
	#Regular expressions
	endOfSeq = re.compile(r"\|\|")
	identRE = re.compile("IDENTIFIERS")
	accessionRE = re.compile('GenBank Acc:\s*(.*)')
	giRE = re.compile('GenBank gi:\s*(.*)')
	sequenceRE = re.compile("SEQUENCE") #outdated
	lastUpdateRE = re.compile("Last Updated:\s*(.*)") #outdated
	libraryRE = re.compile('LIBRARY')
	lib_idRE = re.compile('dbEST lib id:\s*(\d*)')
	descriptionRE = re.compile("Description:")

	#is it me or is this regular expression beautiful?
	attributeRE = re.compile('^(.+?:)\s*(.*)')#Grabs Attribute and value

	#infile
	fh = readfile(infile)

	
	#number of ests finished; this is our visual to make sure program is still running
	count = 0
	sys.stdout.write("Parsing ESTs...	   |<~100%\n")

	#priming for loop
	line = fh.readline()
	#keep everything NA until it is found
	curAcc = "NA"
	curGI = "NA"
	curSequence = "NA"
	curUpdate = "NA"
	curLibID = "NA"
	curAttrs = ['NA']

	while line != "":
		if identRE.match(line):#grab the identifiers
			line = fh.readline()#Skip the blank line after IDENTIFIERS
			line = fh.readline()
			while line != '\n':#parse until we get to library section
				line = fh.readline()
				if accessionRE.match(line):
					curAcc = accessionRE.match(line).groups()[0]
				elif giRE.match(line):
					curGI = giRE.match(line).groups()[0]
					
		elif libraryRE.match(line):#Lib Description attributes
			line = fh.readline()
			curAttrs = []
			while line != '\n':
					try:
						#Description: is on multiple lines and is the last attribute in a library
						if descriptionRE.match(line):
							description = line.strip()+" "#collecting DESCRIPTION information
							line = fh.readline()#prime it up
							while line.startswith(' '):#keep reading all the \s\s\smore text info lines
								description = description + line.strip() + " "
								line = fh.readline()
							#Full description now on one line
							curAttrs.append(description)
						elif lib_idRE.match(line):
							curLibID = lib_idRE.match(line).groups()[0]
						else:
							(NAME,VALUE) = attributeRE.match(line).groups()
							curAttrs.append(NAME+VALUE)
					except Exception:
						#this debug tells us that every line underneath labratory
						#doesn't have the same Label: Value structure
						line = fh.readline()
						continue

					line = fh.readline()

		elif endOfSeq.match(line):#finished an est. Save stuff
			temp = curGI
			temp += "`"
			temp += curAcc
			temp += "`"
			temp += curLibID
			temp += "`"
			temp += "`".join(curAttrs)
			ret.append(temp)

			#Reset all variables
			curAcc = "NA"
			curGI = "NA"
			curSequence = "NA"
			curUpdate = "NA"
			curLibID = "NA"
			curAttrs = ['NA']
			count += 1
			if count % 72856 == 0:#progress visual
				sys.stdout.write('X')
			sys.stdout.flush()

		line = fh.readline()
		
	fh.close()
	fout = open("FormattedDB.pickle",'w')
	pickle.dump(ret,fout)
	fout.close()
	sys.stdout.write('\nFinished!...\n')
	return ret

def buildDatabaseTables(allESTs):
	"""Takes the list that is returned from extractDBinformation
	and puts it in the following format so that it can be loaded into
	a database.

	<library name="required" desc="also required" dbESTid="A Number">
		<attribute label="label">Value</attribute>
		<attribute label="label">Value</attribute>
		<attribute label="label">Value</attribute>
		<members>
			<est name="estName" gi="genbank" acc="acc" />
			<est name="estName" gi="genbank" acc="acc" />
			<est name="estName" gi="genbank" acc="acc" />
			<est name="estName" gi="genbank" acc="acc" />
		</members>
	</library>

	The output of this can be put into ArabiTagDB.py when you want to load the information.
	"""
	

	#For keeping track of what we have and haven't seen.
	myLibraries = {} # line:[Attributes, (estName, estGi, estAcc),(estName, estGi, estAcc) ...]
	for curEST in allESTs:
		#Clean the line
		curEST = re.sub("\"", "'", curEST)
		
		#Split the line
		curData = curEST.split('`')
		curGi = curData[0]
		curAcc = curData[1]
		curDbESTlibID = curData[2]
		curAttributes = curData[3:]

		try:
			myLibraries[curDbESTlibID].append([curGi, curAcc])
		except KeyError:
			myLibraries[curDbESTlibID] = [curAttributes, [curGi, curAcc] ]

	fout = open("GenBankLibs.xml", 'w')
	
	##Setting up templates
	libHead = '<library name="%s" desc="%s" dbESTid="%s">\n'
	attributeLine = '\t<attribute label="%s">%s</attribute>\n'
	estLine = '\t\t<est name="%s" gi="%s" acc="%s" />\n'
	
	for curLib in myLibraries.keys():
		#going to make a dictionary of all the lib's atts
		attributes = {}
		
		#setting up attributes
		for curAtt in myLibraries[curLib][0]:
			dat = curAtt.split(':')
			attributes[dat[0]] = " ".join(dat[1:])#the 1: is just in case there is a : in the value
		
		#writing the <library> line Note: Every Library has a description!
		try:
			fout.write(libHead % (attributes["Lib Name"], attributes["Description"], curLib ) )
			del(attributes["Lib Name"])
			del(attributes["Description"])
		except KeyError:
			fout.write(libHead % (attributes["Lib Name"], "", curLib ) )
		
		#Now write the rest of the attributes
		for curAtt in attributes:
			fout.write(attributeLine % (curAtt, attributes[curAtt] ) )
		
		#Time to write the ESTs
		fout.write('\t<members>\n')
		
		for curEST in myLibraries[curLib][1:]:
			fout.write(estLine % (curEST[1], curEST[0], curEST[1]) )
		fout.write('\t</members>\n')
		fout.write('</library>\n')
		#Boosh. I'm done. Like magic.
		
	fout.close()

if __name__ == "__main__":

	#run Arabidopsis_dbEST_new.py to get the file sys.argv[1] used below
	print "Extracting Information"
	allESTs = extractDBinformation(sys.argv[1])
	print "Creating GenBankLibs.xml"
	buildDatabaseTables(allESTs)
	print "Finished! \n\n Use the below script in MySQL to load statements similar to those below to add data\n"
	print """
LOAD DATA LOCAL INFILE 'Libraries.sqlTable' INTO TABLE Library FIELDS TERMINATED BY '`';
LOAD DATA LOCAL INFILE 'EST_Record.sqlTable' INTO TABLE EST_Record FIELDS TERMINATED BY '`';
LOAD DATA LOCAL INFILE 'Attributes.sqlTable' INTO TABLE Attribute FIELDS TERMINATED BY '`';
LOAD DATA LOCAL INFILE 'Library_Attribute_Value.sqlTable' INTO TABLE LibAttrValue FIELDS TERMINATED BY '`';
"""