
#	The following algorithm was adapated to python
#	by Adam C. English from
#	
#	Loader, Catherine. "Fast and Accurate Computation of Binomial Probabilities" (2000).

import math

PI2 = 6.283185307179586476925289;
S0 = 1/12.0;
S1 = 1/360.0;
S2 = 1/1260.0;
S3 = 1/1680.0;
S4 = 1/1188.0;
	
sfe = [0, 0.081061466795327258219670264, 0.041340695955409294093822081, 0.0276779256849983391487892927, 0.020790672103765093111522771, 0.0166446911898211921631948653, 0.013876128823070747998745727, 0.0118967099458917700950557241, 0.010411265261972096497478567, 0.0092554621827127329177286366, 0.008330562322262871256469318, 0.0075736754879518407949720242, 0.006942840107209529865664152, 0.0064089941880042070694396310, 0.005951370112758847735624416, 0.0055547335519628013710386899];


def stirlerr(n):
		if (n<16): return sfe[int(n)];
		nn = float(n);
		nn = nn*nn;
		if(n >500): return (S0-S1/nn)/n;
		if (n>80):  return (S0-(S1-S2/nn)/nn)/n;
		if (n>35):  return (S0-(S1-(S2-S3/nn)/nn)/nn)/n;
		
		return (S0-(S1-(S2-(S3-S4/nn)/nn)/nn)/nn)/n;
	
def bd0(x, np):
	if (abs(x-np) < 0.1 * (x+np)):
		s = (x-np)*(x-np)/(x+np);
		v = (x-np)/(x+np);
		ej = 2*x*v;
		j=1;
		while(True):
			ej *= v*v;
			s1 = s+ej/(2*j+1);
			if (s1==s): return(s1);
			s = s1;
			j+=1;
		
	return (x*math.log(x/np)+np-x);
	
def dbinom(x, n, p):
	if (p==0.0):
		if x==x:
			return 1.0 
		else:
			return 0.0
	if (p==1.0):
		if x==n:
			return 1.0
		else:
			return 0.0
	if (x==0):
		return math.exp(n*math.log(1-p));
	if (x==n):
		return math.exp(n*math.log(p));

	lc = stirlerr(n) - stirlerr(x) - stirlerr(n-x) - bd0(x,n*p) - bd0(n-x,n*(1.0-p));
	return math.exp(lc)*math.sqrt(n/(PI2*x*(n-x)));
	
def dpois(x, lb):
	if (lb==0):
		if x==0:
			return 1.0 
		else:
			return 0.0
	
	if (x==0):
		return math.exp(-lb);
		
		
	return math.exp(-stirlerr(x) - bd0(x,lb))/math.sqrt(PI2*x);


