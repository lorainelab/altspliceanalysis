#! /usr/bin/python
"""
Let's say you do a new sequencing experiment and you want to let these ESTs
support existing AS Events. Here's the process:

1) Run ArabiTagMain.py on the new ESTs to create AltSplicing.abt

2) Be sure you have you library information xml file made (see BuildLibraryInfo.py for help)

3) Run this file

"""
import MySQLdb
import sys, time, re, gzip, zipfile
import dbinom

__author__="ACEnglish"
__date__ ="$Sep 29, 2009 11:50:47 AM$"

def readfile(f):
    """
    Function: Open a file for reading.
    Returns : a file handle
    Args    : the name of a file

    The given file can be compressed (file extension .gz)
    or a regular non-compressed file.

    """
    reg = re.compile(r'\.gz$')
    reg2 = re.compile(r'\.zip$')
    if reg.search(f):
        z = gzip.GzipFile(f)
    elif zipfile.is_zipfile(f):
        return None
    else:
        z = open(f)
    return z
		
def getSpliceTypeAndDifferenceCoords(spliceType,iOverlap,eOverlap):
	"""
	returns the differance region of an alternative splicing event and what type
	of alternative splicing the event is.

	Types of Alternative Splicing -All Examples are + strand-

	Acceptor Site   XX---XXXX
					XX----XXX

	Donor Site	  XXX-----XX
					XXXXX---XX

	Shift		   XXXXX----XXXXX
					XXX----XXXXXXX

	Retained Intron XXXX----XXX
					XXXXXXXXXXX

	Exon Skipping   ----XXX----
					-----------

	This method returns
	-What type of AS event this is, Difference in: Acceptor Site, Donor Site, Shift, Retained Intron, Exon Skipping
	-The coordinates of the AS event as a string "Start,End"
		*if difference is a shift, We'll return both locations as "Start1,End1;Start2,End2"
	"""

	(iStart, iLength, iStrand) = iOverlap.split(',')
	iStart = int(iStart)
	iLength = int(iLength)
	iEnd = iStart + iLength

	(eStart, eLength, eStrand) = eOverlap.split(',')
	eStart = int(eStart)
	eLength = int(eLength)
	eEnd = eStart + eLength

	diff = str(max(iStart,eStart))+','+str(min(iEnd,eEnd))

	return (spliceType,diff)

def addLibrary(db, library):
	"""
	Given a database connection and a library.xml, create a new library.
	
	We know that AttributeId 1 is Name and AttributeId 2 is description and those are the only two things REQUIRED for a library
	
	We also don't want redundant AttributeIds, so we have to query the existing labels and use their ids

	Also, we'll use this step to add all the ESTs, which are in the Library.xml
	
	ToDo this we will:
	1) Grab match the library head line and extract name and desc and genbank
	2) Do the Attribute Check/Entering
	3) When we get to <members> add every line
		a) Different regular expressions if we're in genbank or regular type
	4) we'll get to </members>
	5) we'll get to </library>
	6) Repeat until we're on the last line
	
	library.xml format	
	<library name="required" desc="also required" genbank="TRUE OR FALSE">
		<attribute label="label">Value</attribute>
		<attribute label="label">Value</attribute>
		<attribute label="label">Value</attribute>
		<members>
			<est name="estName" gi="genbank" acc="acc" />
			<est name="estName" gi="genbank" acc="acc" />
			<est name="estName" gi="genbank" acc="acc" />
			<est name="estName" gi="genbank" acc="acc" />
		</members>
	</library>
	"""
	
	#get existing labels to prevent redundant information
	curs = db.cursor()
	curs.execute("SELECT * FROM Attribute")
	
	usedLabels = {}# Label:Id - so there are no repeats
	for x in curs.fetchall():
		usedLabels[x[1]] = x[0]
	
	#regular expression pulls
	libraryRe = re.compile('<library name="(.*)" desc="(.*)" dbESTid="(.*)">')
	libraryEndRe = re.compile('\s*</library>')
	attributeRe = re.compile('\s*<attribute label="(.*)">(.*)</attribute>')
	membersRe = re.compile('\s*<members>')
	membersEndRe = re.compile('\s*</members>')
	estNonGbRe = re.compile('\s*<est name="(.*)" />')
	estGbRe = re.compile('\s*<est name="(.*)" gi="(.*)" acc="(.*)" />\n')
	
	#insertion queries
	libQuery = "INSERT INTO Library (information, genbank) VALUES "
	attributesQuery = "INSERT INTO Attribute (Label) VALUES "
	libAttrValQuery = "INSERT INTO LibAttrValue (LibraryId, AttributeId, LAvalue) VALUES (%s, %s, %s) "
	estsQuery = "INSERT INTO EST_Record (estName, LibraryId, GenBank_gi, GenBank_Acc) VALUES (%s, %s, %s, %s)"

	
	
	#This is where I'll keep all the ests I just entered information
	ret = {}#estName:(estId, libId)
	
	fh = open(library,'r')
	line = fh.readline()
	while line != "":
		if libraryRe.match(line):#We have a new Library
				sys.stdout.write("Adding Library: ")
				libAttrVals = []#list of all the library attribute values, I need this to make library information	
				(name, desc, dbESTid) = libraryRe.match(line).groups()
				sys.stdout.write(name+"\n")
				libAttrVals.append( ("LibName",1,name))
				libAttrVals.append( ("Description",2,desc) )
				if dbESTid == 'Null':
					myEST_Re = estNonGbRe
					genbank = "FALSE"
				else:
					myEST_Re = estGbRe
					libAttrVals.append( ("dbESTid",3,dbESTid) ) 
					genbank = "TRUE"
		elif attributeRe.match(line): #make libattrval and attribute
				(label, value) = attributeRe.match(line).groups()
				try:
					ident = usedLabels[label]
				except KeyError:#new label, insert it
					curs.execute(attributesQuery + "('%s')" % label)
					ident = curs.lastrowid
					usedLabels[label] = ident#add to my dictionary
				libAttrVals.append( (label, ident, value) )
		elif membersRe.match(line): #we're in the ESTs
			#First, let's add the library information
			#make library information
			libInfo = name + " " + desc 
			for x in libAttrVals:
				libInfo += " " + x[0] + " " + x[2]
			
			#insert library
			curs.execute(libQuery + '("%s", %s)' % (libInfo, genbank))
			libId = curs.lastrowid
			
			#insert all library attribute values
			vals = []
			for x in libAttrVals:
				vals.append((libId, x[1], x[2]))
			
			curs.executemany(libAttrValQuery, vals)

			#Now, let's grab all of our ESTs and put them in
			estVals = []
			line = fh.readline() #primer readline
			while not membersEndRe.match(line):
				try:
					data = myEST_Re.match(line).groups()
				except AttributeError:
					print line, "Your Library.xml file is corrupt. Ensure it is in the correct format and try again."
					sys.exit(1)
				
				#There can be two different sets returned, so I need to ensure I'm ready for both with try statements.
				estName = data[0]#Guaranteed one
				try:
					estGI = data[1]
					estAcc = data[2]
				except IndexError:
					estGI = "NULL"
					estAcc = "NULL"
				
				estVals.append((estName, str(libId), estGI, estAcc))
				line = fh.readline()
			print "\tAdding Library's ESTs"
			curs.executemany(estsQuery, estVals)
			#Build our DB dictionary of {estName: (EST_Id, LibId)}
			curs.execute("SELECT estName, ESTId FROM EST_Record WHERE LibraryId = %s" % libId)
			for x in curs.fetchall():
				ret[x[0]] = ( x[1], libId )
				
		elif libraryEndRe.match(line):
			#Reset our variables just for shits and giggles.
			del(myEST_Re)
			del(libId)
			del(libAttrVals)
			del(libInfo)
			del(estVals)
			del(vals)
			del(data)
			
			
		line = fh.readline()
	
	curs.close()
	
	return ret

	
def addAltSplicing(db, altsplice, estInfo):
	"""
	Given a database connection, a .abt file and estInfo {estName:ESTId}
	
	Make a dictionary of all the AS events so we can get the ASid - retrieveAS_EventInformation - method in this file
	
	Parse the altsplice file and add to the AS_Choice_Support Table - need to make parse / insertAS_ChoiceSupport 
	
	Then Run a database script that will recalculate the AS_Support_Count table - cleanAS_ChoiceSupport
	
	Then run a script that updates the Exon Skipping [ES_Events] table. - need to make
	
	NOTE: to remove pre-post-over processing steps, we're taking a .abt straight from AltSplicing/ArabiTagMain.py - therefore, we need to calculate the difference region and the correct AS type (dependent on strand)
	
	#for each as event in the .abt file
		#is the event in the database?
			#add to the AS count, add that the library supports the event
		#else
			#detailed error message saying that the as event isn't already in the db, did you use the same versions of the reference genome to build the .abt file as you used to build the 
	"""
	
	#geneAbsentId,genePresentId,diffRegionGenomicStart,diffRegionGenomicEnd,type,AS_Id 
	#grab existing events
	
	as_events = retrieveAS_EventInformation(db)
	curs = db.cursor()

	fh = readfile(altsplice)
	asSupportInsert = []#all the data we're going to insert into as_choicesupport

	#geneAbsentId,genePresentId,diffRegionGenomicStart,diffRegionGenomicEnd,type
	for line in fh.readlines():
		(locus,Ga,Gp,iOverlap,eOverlap,iAltOverlap,oType,GaESTs,GpESTs) = line.split('\t')
		GpESTs = GpESTs.strip()
		(type,diff) = getSpliceTypeAndDifferenceCoords(oType,iOverlap,eOverlap)
		(gStart,gEnd) = diff.split(',')
		
		if ",".join([Ga,Gp,gStart,gEnd,type]) in as_events:
			AS_Id = as_events[",".join([Ga,Gp,gStart,gEnd,type])]
		else:
			curs.execute("INSERT INTO AS_Event (geneAbsentId, genePresentId, diffRegionGenomicStart, diffRegionGenomicEnd, type) VALUES ('%s', '%s', %s, %s, '%s')" % (Ga, Gp, gStart, gEnd, type))
			AS_Id = curs.lastrowid
		
		for est in GaESTs.split(','):
			if est != "":
				if est.startswith('gb|'):
					est = est[3:]#Getting rid of genbank gb crap
				try:
					(estId, libId) = estInfo[est]
				except KeyError:
					sys.stderr.write("There is an EST (%s) that supports an Alternative Splicing Event, but doesn't belong to a library you just added! Make sure you've properly created your Library.xml file!" % est)
					sys.exit(1)
				asSupportInsert.append( (AS_Id,Ga,estId,libId) )
				
		for est in GpESTs.split(','):
			if est != "":
				if est.startswith('gb|'):
					est = est[3:]#Getting rid of genbank gb crap
				try:
					(estId, libId) = estInfo[est]
				except KeyError:
					sys.stderr.write("There is an EST (%s) that supports an Alternative Splicing Event, but doesn't belong to a library you just added! Make sure you've properly created your Library.xml file!" % est)
					sys.exit(1)
				asSupportInsert.append( (AS_Id,Gp, estId, libId) )

	#do the as_choicesupport insert	
	curs.executemany('INSERT INTO AS_ChoiceSupport(AS_Id, GeneId, ESTId, LibraryId) VALUES (%s, %s, %s, %s) ', asSupportInsert)
	curs.close()
	cleanAS_ChoiceSupport(db)

def updatePValues(db):
	"""
	Given a database connection to a database that just:
	had ESTs added
	had AS_ChoiceSupport updated
	had AS_Support_Count updated
	
	go through AS_Support_Count#Database Retrieve
	
	for each AS_ID, 
		calculate dbinom for ga_count, gp_count, .5 #dbinom step
		update AS_ID.pvalue #Database Step
	"""	
	curs = db.cursor()
	
	curs.execute("SELECT AS_Id, gaCount, gpCount FROM AS_Support_Count")
	
	rows = []
	for x in curs.fetchall():
		if int(x[1]) + int(x[2]) == 0:
			continue
		pval = dbinom.dbinom(int(x[1]),int(x[1])+int(x[2]),.5)
		rows.append( (pval,x[0]) )
	
	for x in rows:
		curs.execute("UPDATE AS_Event SET PValue = %f WHERE AS_Id = %s;" % (x[0],x[1]))
	
	db.commit()
	curs.close()
	
def retrieveAS_EventInformation(db):
	"""
	retrieves
		AS_Event information - AS_Id
	using
		AltSplicing - Ga, Gp, genomicDiffStart, genomicDiffEnd, Type

	creates a dictionary of structure

	geneAbsentId,genePresentId,diffRegionGenomicStart,diffRegionGenomicEnd,type:AS_Id
	"""
	q = "SELECT geneAbsentId,genePresentId,diffRegionGenomicStart,diffRegionGenomicEnd,type,AS_Id FROM AS_Event"
	curs = db.cursor()
	curs.execute(q)
	results = curs.fetchall()
	ret = {}
	for row in results:
		t = row[4]
		if len(t) > 2:
			t = t[:2]
		key = "%s,%s,%i,%i,%s" % (row[0],row[1],row[2],row[3],t)
		ret[key] = row[-1]

	curs.close()
	return ret

def insertAS_ChoiceSupport(db,data):
	"""
	updates
		AS_ChoiceSupport - whole new row
	using
		AS_Id, GeneId, EST_Id, LibraryId

	takes in a list of lists
	[[AS_Id,GeneId,EST_Id,LibraryId],[AS_Id,GeneId,EST_Id,LibraryId]]
	"""
	q = "INSERT INTO AS_ChoiceSupport(AS_Id,GeneId,ESTId,LibraryId) VALUES "

	for vals in data:
		q += "(%i,'%s',%i,%i) , " %(vals[0],vals[1],vals[2],vals[3])

	q = q[:-2]#truncate the last comma.
	curs = db.cursor()
	curs.execute(q)
	curs.close()

def cleanAS_ChoiceSupport(db):
	"""
	runs create as choice support.sql to update the table of counts
	"""
	
	q = """CREATE TABLE real_choiceSupport AS SELECT * FROM AS_ChoiceSupport WHERE 1 GROUP BY AS_Id, GeneId, ESTId, LibraryId;
	Drop Table AS_ChoiceSupport;
	RENAME TABLE real_choiceSupport TO AS_ChoiceSupport;
	DROP TABLE AS_Support_Count;
	CREATE TABLE AS_Support_Count(AS_Id INT, gaCount INT, gpCount INT, PRIMARY KEY(AS_Id), FOREIGN KEY(AS_Id) REFERENCES AS_Event(AS_Id) ON UPDATE CASCADE ON DELETE CASCADE)ENGINE=INNODB;
	#for all the AS events
	INSERT INTO AS_Support_Count(AS_Id) SELECT AS_Id FROM AS_Event;
	CREATE OR REPLACE VIEW TEMP AS SELECT e.AS_Id, COUNT(e.genePresentId) AS gpCount FROM AS_Event e, AS_ChoiceSupport c WHERE (e.AS_Id = c.AS_Id) AND e.genePresentId = c.GeneId GROUP BY e.AS_Id;	
	UPDATE AS_Support_Count a, TEMP t SET a.gpCount = t.gpCount WHERE a.AS_Id = t.AS_Id;
	UPDATE AS_Support_Count SET gpCount = 0 WHERE gpCount IS NULL;
	CREATE OR REPLACE VIEW TEMP AS SELECT e.AS_Id, COUNT(e.geneAbsentId) AS gaCount FROM AS_Event e, AS_ChoiceSupport c WHERE (e.AS_Id = c.AS_Id) AND e.geneAbsentId = c.GeneId GROUP BY e.AS_Id;
	UPDATE AS_Support_Count a, TEMP t SET a.gaCount = t.gaCount WHERE a.AS_Id = t.AS_Id;
	UPDATE AS_Support_Count SET gaCount = 0 WHERE gaCount IS NULL; 
	DROP VIEW TEMP;
	CREATE INDEX index1 ON AS_ChoiceSupport (AS_Id, GeneId);
	CREATE INDEX index2 ON AS_ChoiceSupport (AS_Id, LibraryId);
	CREATE INDEX GeneId on AS_ChoiceSupport (GeneId);
	CREATE INDEX ESTId on AS_ChoiceSupport (ESTId);
	CREATE INDEX LibraryId on AS_ChoiceSupport (LibraryId);"""
	
	curs = db.cursor()
	
	for x in q.split('\n'):
		curs.execute(x)
		db.commit()
		time.sleep(3)
		
	updatePValues(db)
		
def updateExonSkipping(db):
	"""
	Creates AS Types that are part of ES
	"""
	
	query = """CREATE OR REPLACE VIEW ESpart AS SELECT A.AS_Id FROM AS_Event A, AS_Event P WHERE A.geneAbsentId = P.geneAbsentId AND A.genePresentId = P.genePresentId AND A.diffRegionGenomicStart = P.diffRegionGenomicStart AND A.diffRegionGenomicEnd = P.diffRegionGenomicEnd AND A.type != P.type;
			UPDATE AS_Event A, ESpart E SET A.type = concat(A.type,'/ES') WHERE A.AS_Id = E.AS_Id;
			DROP VIEW ESpart;"""
	
	curs = db.cursor()
	for x in query.split('\n'):
		curs.execute(x)
	curs.close()
	
def main(altsplice, library, host, database, user, passwd):
	"""
	altsplice - the name of the AltSplicing.abt file
	library- the name of the library.xml file
	user - the database username
	passwd - the database username's password
	"""
	db = MySQLdb.connect(host=host,user=user,passwd=passwd,db=database)
	##First, add the library #NOTE!
	estInfo = addLibrary(db, library)
	db.commit()
	print "Adding AS Events"
	##Finally, add the altSplicing
	addAltSplicing(db, altsplice, estInfo)
	db.commit()
	print "Cleaning AS Events Support and Count"
	cleanAS_ChoiceSupport(db)
	db.commit()
	print "Updating Exon Skipping"
	updateExonSkipping(db)
	db.commit()
	db.close()
	print "Finished!"


"""
Drop Table LibAttrValue;
Drop Table News;
Drop Table AS_ChoiceSupport;
Drop Table AS_Support_Count;
Drop Table ESTSupportGene;
Drop Table AS_Event;
Drop Table EST_Record;
Drop Table Library;
Drop Table GeneModel;
Drop View LAViewer;
Drop View LibOverview;
"""
