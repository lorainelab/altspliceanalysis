####File Formats####

bed file labels:
chrom	chromStart	chromEnd	name	score	strand		thickStart thickEnd	itemRgb	blockCount	blockSizes	blockStart

the abt file should have following information in the header:
1: locus				ex: LOC_Os01g01720	
2: splice variant 1			ex: LOC_Os01g01720.2
3: splice variant 2			ex: LOC_Os01g01720.1
4a: start intron v1			ex: 383861
4b: v1 intron size			ex: 88
4c: direction of read			ex: -1
5a: location of exon (including UTR)	ex: 383046
5b: overlapping area			ex: 818
5c: direction of read			ex: -1
6a: start of v2 intron read		ex: 383864
6b: v2 intron size			ex: 85
6c: direction of read			ex: -1
7:  class of alt splice event		ex: AS

With the following header layout:
1	2	3 4a,4b,4c	5a,5b,5c	6a,6b,6c	7


"""""""""Tests""""""""""

####retained intron####

#test_loc.1
Chromosomal position:	0 10 20 30 40 50 60 70 80 90 100 110
			_ x  X  X  _  _  _  X  X  X  x   _
Position within gene:	_ 0 10 20 30 40 50 60 70 80 90 100 
	.bed notation: test_chrom	10	110	test_loc.1	0	+	20	100	0	2	30,40	0,60	test_loc

#test_loc.2
Chromosomal position:	0 10 20 30 40 50 60 70 80 90 100 110			
			_ x  X  X  X  X  X  X  X  X  x   _
Position within gene:	_ 0 10 20 30 40 50 60 70 80 90  100 
	.bed notation: test_chrom	10	110	test_loc.2	0	+	20	100	0	1	100,	0,	test_loc

.bed file:
test_chrom	10	110	test_loc.1	0	+	20	100	0	2	30,40	0,60	test_loc
test_chrom	10	110	test_loc.2	0	+	20	100	0	1	100,	0,	test_loc

.abt file
test_loc\ttest_loc.1\ttest_loc.2\t40,30,1\t10,100,1\tNA\tRI


####retained_intro_LOC_Os01g01040####
This comes from IGB’s data about LOC_Os01g01040 from Oryzo sativa Japonica
Chr1	16320	20323	LOC_Os01g01040.2	0	+	16598	19593	0	8	656,92,701,71,90,180,99,593,	0,1062,1237,2180,2647,2821,3210,3410,	LOC_Os01g01040	expressed protein
Chr1	16320	20323	LOC_Os01g01040.3	0	+	16598	19593	0	7	656,92,701,71,90,180,793,	0,1062,1237,2180,2647,2821,3210,	LOC_Os01g01040	expressed protein


####Alternative Donor#######
This is pulled directly from IGB’s data about LOC_Os01g01720 from Oryzo sativa Japonica

.bed file
e
Chr1	383046	386701	LOC_Os01g01720.1	0	-	383450	386548	0	10	818,71,46,32,150,184,176,49,200,187,	0,903,1084,1255,1362,1619,1887,2533,2677,3468,	LOC_Os01g01720	pex14, putative, expressed
Chr1	383046	386701	LOC_Os01g01720.2	0	-	383450	386548	0	10	815,71,46,32,150,184,176,49,200,187,	0,903,1084,1255,1362,1619,1887,2533,2677,3468,	LOC_Os01g01720	pex14, putative, expressed



