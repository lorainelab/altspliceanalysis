#!/usr/bin/env python

"""
Given a list of junction BED files output by TopHat, reformat for ArabiTagMain.py.

The output will be a single .bed file, with junction BED files prefix used for the id
field.

Example:

chrC	11883	12722	CoolHT2^8	8	+	11883	12722	255,0,0
2	55,69	0,770
"""

import sys, time, re, os, zipfile, gzip

from optparse import OptionParser

def getSpliceTypeAndDifferenceCoords(type,iOverlap,eOverlap):
    """
    returns the differance region of an alternative splicing event and what type
    of alternative splicing the event is.

    Types of Alternative Splicing -All Examples are + strand-

    Acceptor Site   XX---XXXX
                    XX----XXX

    Donor Site      XXX-----XX
                    XXXXX---XX

    Shift           XXXXX----XXXXX
                    XXX----XXXXXXX

    Retained Intron XXXX----XXX
                    XXXXXXXXXXX

    Exon Skipping   ----XXX----
                    -----------

    This method returns
    -What type of AS event this is, Difference in: Acceptor Site, Donor Site, Shift, Retained Intron, Exon Skipping
    -The coordinates of the AS event as a string "Start,End"
        *if difference is a shift, We'll return both locations as "Start1,End1;Start2,End2"
    """

    (iStart, iLength, iStrand) = iOverlap.split(',')
    iStart = int(iStart)
    iLength = int(iLength)
    iEnd = iStart + iLength

    (eStart, eLength, eStrand) = eOverlap.split(',')
    eStart = int(eStart)
    eLength = int(eLength)
    eEnd = eStart + eLength



    if type == 'DS' and iStrand == -1:
        type = 'AS'
    elif type == 'AS' and iStrand == -1:
        type = 'DS'

    diff = str(max(iStart,eStart))+','+str(min(iEnd,eEnd))


    return (type,diff)

def readfile(f):
    """
    Function: Open a file for reading.
    Returns : a file handle
    Args    : the name of a file

    The given file can be compressed (file extension .gz)
    or a regular non-compressed file.

    """
    reg = re.compile(r'\.gz$')
    reg2 = re.compile(r'\.zip$')
    if reg.search(f):
        z = gzip.GzipFile(f)
    elif zipfile.is_zipfile(f):
        return None
    else:
        z = open(f)
    return z


if __name__ == '__main__':
    usage = "usage: %prog [options] file1.bed [file2.bed, ... ]"
    parser = OptionParser(usage)
    parser.add_option("-s","--suffix",dest="suffix",help="suffix for output file [required]",
                      metavar="STRING",default=None)
    (options, args) = parser.parse_args()
    suffix = options.suffix
    files = args
    if len(files)==0 or not suffix:
        parser.print_help()
        sys.exit(1)
    sys.stderr.write("About to read files: %s.\n"%",".join(files))
    outfn = 'allJunctions%s.bed'%suffix
    sys.stderr.write("About to write: %s.\n"%outfn)
    fout = open(outfn, 'w')
    for file in files:
        lib_name=file.split('.')[0]
        fh = readfile(file)
        for line in fh.readlines():
            if line.startswith('track'):
                sys.stderr.write("Read track line: %s"%line)
                continue
            line = line.split('\t')
            score = int(float(line[4])) # scores supposed to be floats in BED12
            line[3] = lib_name+"^"+str(score)
            fout.write("\t".join(line))
        fh.close()
    fout.close()

