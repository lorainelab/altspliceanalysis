"""
Converts output of running ArabiTagMain.py with ordinary ESTs or mRNAs to
format accepted by PostParse.py.

Run in a pipeline like so:

$ ArabiTagMain.py -g TAIR10.bed -e A_thaliana_Jun_2009.EST.sm.psl -s ESTF20 -f 20

Produces two files:

 AltSplicing_ESTF20.abt 
 ESTSupportsLocus_ESTF20.txt

Convert AltSplicing_EST20.abt to format accepted by PostParse.py.

$ ConvertToLibraryFormat.py --lib_name ESTF20 AltSplicing_ESTF20.abt AltSplicing_ESTF20_LIB.abt

Then, run:

$ PostParse.py AltSplicing_ESTF20_LIB.abt AltSplicing_ESTF20_LIB.txt

"""

import optparse

if __name__ == '__main__':
    usage = """%prog [options] infile outfile

    example)

    $ ConvertToLibraryFormat.py --lib_name ESTF20 AltSplicing_ESTF20.abt AltSplicing_ESTF20_LIB.abt

    or

    $ ConvertToLibraryFormat.py AltSplicing_ESTF20.abt AltSplicing_ESTF20_LIB.abt

    in this case, library name will be ESTF20, formerly the -s (suffix) given to ArabiTagMain.py
    """

    parser = optparse.OptionParser(usage)
    parser.add_option('-l','--lib_name',dest="lib_name"
                      help="Name of library. Uses .abt file suffix if not provided [optional]",
                      type=string,metavar="STRING",default=None)
    (options,args)=parser.parse_args()
    if not len(args) == 2:
        parser.print_help()
        sys.exit(1)
    infile = args[0]
    outfile = args[1]
    lib_name=options.lib_name
    if not lib_name:
        try:
            lib_name = infile.split('AltSplicing_')[1]
        except IndexError:
            parser.print_help()
            sys.exit(1)
    main(infile=infile,outfile=outfile,lib_name=lib_name)

        
