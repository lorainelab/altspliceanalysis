#!/usr/bin/env python
"""
This file adds retained intron support to the Gp for all the statistics layout file created by PostParse.py

Requires a directory with BAM files whose titles match the headers of the input file from
the PostParse.py program.
"""

import sys, os, commands, optparse

def getRiGpSupport(fn, diffRegion,sm_only=True):
	"""
	Function: Count non-spliced reads that overlap a 
	          a differentially spliced region.
	Args: fn - BAM filename (with path)  
	      diffRegion - differentially spliced region, chr:start-stop
	      sm_only - only use single-mapping reads
	Returns: number of overlapping reads
	"""	
	pat = 'NH:i:1' # single-mapping reads
	retCount = 0
	c = "samtools view %s %s" % (fn, diffRegion)
	sys.stderr.write("Executing: %s\n"%c)
	samLines = commands.getoutput(c).split('\n')
	for line in samLines:
		try:
			toks=line.split('\t')
			if sm_only:
				result=filter(lambda x:x==pat,
					      toks[10:])
				if len(result)==0:
					continue
			read_len=len(toks[9])
			cigar = toks[5]
			s='%iM'%read_len
			if cigar==s:
				retCount += 1
		except IndexError:
			sys.stderr.write("Warning! Bogus line: %s\n"%line)
			pass
	
	return retCount

def parseStatisticsLayoutFile(fn=None,fnout=None,dir=None,
			      FLANK=5,sm_only=True):
	"""
	This takes the statistics file and for each Retained Intron event asks getRiGpSupport for the number of non-intron containing ests within the difference region with a flank. 
	
	format: NOTE!!! There must be a header on the file
	
		chromosome\t strand\t start\t end\t type\t Ga\t Gp\t libraryName's GaCount\t libraryName's GpCount
	fn - input statistics layout file
	fnout - the updated statistics layout file we'll write
	dir - where all the bam files are
	FLANK - amount of overlap required to count as an intron read, default is 5
	"""
	fh = open(fn,'r')
	header = fh.readline().strip().split('\t')
	
	libIndex = {}#this will keep track of all the libs and their index lib:index
	
	for i in range(8,len(header),2):
		libIndex[header[i][3:]] = i#the [3:] removes the Gp_
		
	fout = open(fnout,'w')
	fout.write("\t".join(header)+'\n')#write the header
	for line in fh.readlines():
		curDat = line.strip().split('\t')
		
		if curDat[4] == 'RI':
			chrom = curDat[0]
			start = int(curDat[2])+FLANK
			end = int(curDat[3])-FLANK
			if start > end:
				sys.stderr.write("Warning: start > end for: %s\n"%line.rstrip())
				start = int(curDat[2])
				end = int(curDat[3])
			for lib in libIndex.keys():
				bamfile=dir+os.sep+lib+'.bam'
				numSup = getRiGpSupport(bamfile,
							"%s:%i-%i" % (chrom,start,end),
							sm_only)
				curDat[libIndex[lib]] = str(int(curDat[libIndex[lib]]) + numSup)
		
		fout.write("\t".join(curDat)+"\n")	
				
	fh.close()
	fout.close()
	
	
if __name__ == '__main__':
    usage = "%prog [options]"
    parser = optparse.OptionParser(usage)
    parser.add_option("-d","--dir",
                      help="directory with BAM files. Default is current working directory.",
                      dest="dir",default=".")
    parser.add_option("-i","--in_file",dest="fn",
		      help="file from PostParse.py")
    parser.add_option("-o","--out_file",dest="out_file",
		      help="file to write")
    parser.add_option("-f","--flank",dest="flank",default=5,
		      help="overhang (flank) length required",type="int")
    parser.add_option("-s","--single_mapping_only",dest="sm_only",
		      action="store_true",default=True,
		      help="Only use single-mapping reads. Default.")
    (options,args)=parser.parse_args()
    if not options.fn:
	    parser.error("Please provide file to read.")
    if not options.out_file:
	    parser.error("Please provide name of file to write.")
    if not options.dir:
	    parser.error("Please specify a directory containing BAM files.")
    parseStatisticsLayoutFile(fn=options.fn,
			      fnout=options.out_file,
			      dir=options.dir,
			      sm_only=options.sm_only,
			      FLANK=options.flank)
	
	
	
	
	
