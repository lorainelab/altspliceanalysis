### Introduction

This is our code to setup TopHat Junctions.bed for quick feeding into
ArabiTag and a post-parse that will give us a new format for easy
statistical analysis.

After running everything, the output format is:

chromosome\t strand\t start\t end\t type\t Ga\t Gp\t 

and then Ga and Gp counts for one or more libraries.

To run:

(1) PreParse.py -s [suffix] file.bed [file.bed, ...]

ex) 

PreParse.py -s _hot_cool Cool*bed Hot*bed

creates allJunctions_hot_cool.bed 

(2) ArabiTagMain.py -g models[.psl|bed] -e allJunction[suffix].bed -s SUFFIX

where models is a PSL or BED format file containing gene structures and SUFFIX
will be added to the output file, e.g., AltSplicing_SUFFIX.abt

ex) ArabiTagMain.py -g TAIR10.bed.gz -e allJunctions_hot_cool.bed -s hot_cool

will create output file Altsplicing_hot_cool.abt

(3) PostParse.py AltSplicing_[SUFFIX].abt FILE

  where *.abt is output from ArabiTagMain.py (previous step)

Afterwards, if you want to get Retained Intron Support, you should run
Add_RiGp_Support with the outfile from PostParse.py. Read the file for
instructions on how to run.

Notes left over from Adam English:
____________________________________________________
Here is the basic workflow I've used to create the TopHatAltSplice Statistics and the RIGP.bed files.

>>> python extractSAM_RIGP_Support.py -b accepted_hits.bam -r ArabiTag_RI.csv -f20 -m75 > RIGP.bed 
#Where 
	accepted_hits.bam is the TopHat output with the alignments you care about
	
	ArabiTag_RI.csv is the ArabiTagOnline csv output from AS Event search NOTE!! ArabiTagOnline may be broken! Check your csv output. You may need to grep 'RI' results.txt > ArabiTag_RI.csv
	#####!!!!!!!!See extractSAM_RIGP_support.py -h!!!!!!!!!##############


>>> python PreParse.py -t _known_junctions.bed *_known_junctions.bed
#Where 
	-t some_suffix  is the common suffix to all files that you want removed so that you can get to the prefix of the library name (this is optional, but you probably want it)
		e.x.
			cold_treatment.filtered.bed
			cold_control.filtered.bed
		you would want:
			-t .filtered.bed
	
	*_known_junctions.bed is all the beds you want to parse and add to a single est file that will be used in the next step

>>> python ArabiTagMain.py -g TAIR9_mRNA.bed.gz -e allJunctionsMar162010.bed -f20 -o ../ -s AllJuncMar162010
#Where
	This is your basic ArabiTagMain run. be sure to use the -s for labeling
	
	-e allJunctionsMar162010.bed is the results from previous step (python PreParse.py)
	
	#!See ArabiTagMain.py -h for more info!#

>>> python PostParse.py AltSplicing_AllJuncMar162010.abt TopHatJunctionStatistics.txt
#Where
	AltSplicing_AllJuncMar162010.abt is the output from the preveious step (python ArabiTagMain.py)
	
	TopHatJunctionStatistics.txt is the output file which is a giant grid stats (See PostParse.py for format and more info)
	
>>> python Add_RiGp_Support.py TopHatJunctionStatistics.txt <dir> TopHatAltSpliceSupport.txt
#Where
	TopHatJunctionStatistics.txt is the output file from the previous step (PostParse.py)
	
	<dir> is the directory to all of the library's .bam files. 
		This means that the library names in the Statistics.txt file need to be the same as the prefix on the library's .bam file
		e.x.
			library names in statistics.txt
				cold_treatment
				cold_control
			library .bam files in <dir>
				cold_treatment.bam
				cold_control.bam
	
	TopHatAltSpliceSupport.txt is the outfile to save the new results


 
