
# Gets test data for running ArabiTag alternative splicing analysis
# Uses data deployed on IGB Quickload.
# Uses alternatively spliced gene LOC_Os01g35580 as an example for analysis.
# This gene is alternatively spliced and differentially alternatively 
# spliced in rice roots and shoots. However, it is not differentially
# spliced in response to cytokinin. (Data are from a study done in the 
# Loraine lab.)
# Uses data deployed at:
# http://igbquickload.org/quickload/O_sativa_japonica_Oct_2011/rnaseq/SRP049054
# requires samtools, tabix in user's PATH
# requires internet connection
R="Chr1:19,681,599-19,686,535"
U="http://igbquickload.org/quickload/O_sativa_japonica_Oct_2011/rnaseq/SRP049054/"
SS="BA2h-R1 BA2h-R2 BA2h-R3 BA2h-S1 BA2h-S2 BA2h-S3 Control2h-R1 Control2h-R2 Control2h-R3 Control2h-S1 Control2h-S2 Control2h-S3"
for S in $SS; do
    echo $S
    samtools view -bh $U/$S.bam $R > $S.bam
    samtools index $S.bam
    # tabix is supposed to be able to retrieve data from URLs, but it can't
    # so download the entire file, get the data, and delete the file
    # this fails: https://bitbucket.org/lorainelab/ricealtsplice
    wget $U/$S.FJ.bed.gz 
    wget $U/$S.FJ.bed.gz.tbi
    tabix -f $S.FJ.bed.gz $R > $S.FJ.bed
    rm $S.FJ.bed.gz*
done

